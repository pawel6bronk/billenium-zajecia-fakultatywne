import React from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

const Button = ({ children, ...props }) => (
   <button className={styles.button} {...props}>
      {children}
   </button>
);

Button.propTypes = {
   children: PropTypes.string,
};

export default Button;
