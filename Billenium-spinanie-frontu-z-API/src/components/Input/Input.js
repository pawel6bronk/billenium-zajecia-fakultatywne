import React from "react";
import PropTypes from "prop-types";
import styles from "./Input.module.scss";

const Input = ({ tag: Tag, type, name, label, ...props }) => (
   <div className={styles.formItem}>
      <Tag
         className={Tag === "textarea" ? styles.textarea : styles.input}
         type={type}
         name={name}
         id={name}
         required
         placeholder=" "
         {...props}
      />
      <label className={styles.label} htmlFor={name}>
         {label}
      </label>
      <div className={styles.formItemBar} />
   </div>
);

Input.propTypes = {
   tag: PropTypes.string,
   type: PropTypes.string.isRequired,
   name: PropTypes.string.isRequired,
   label: PropTypes.string.isRequired,
   maxLength: PropTypes.number,
};

Input.defaultProps = {
   tag: "input",
};

export default Input;
