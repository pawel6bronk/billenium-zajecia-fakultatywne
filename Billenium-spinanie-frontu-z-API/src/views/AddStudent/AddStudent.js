import React from "react";
import styles from "./AddStudent.module.scss";
import { Formik } from "formik";
import axios from "axios";
import Button from "../../components/Button/Button";
import Input from "../../components/Input/Input";

// "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
//   "name": "string",
//   "surname": "string",
//   "index": "string"
const Login = () => {
   const postStudentHandler = (values) => {
      console.log(values);
      axios
         .post(`https://uwm-gr1.azurewebsites.net/api/Student`, values)
         .then((res) => {
            console.log(res);
            alert("Pomyślnie dodano studenta");
         })
         .catch((error) => {
            alert("Error: " + error.response.status);
         });
   };
   return (
      <div className={styles.wrapper}>
         <div className={styles.form}>
            <Formik
               initialValues={{ name: "", surname: "", index: "" }}
               onSubmit={(values, { setSubmitting }) => {
                  setTimeout(() => {
                     postStudentHandler(values);
                     setSubmitting(false);
                  }, 400);
               }}
            >
               {({
                  values,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  /* and other goodies */
               }) => (
                  <form onSubmit={handleSubmit}>
                     <h1>Witaj w UWM</h1>
                     <div className={styles.form__content}>
                        <Input
                           type="text"
                           name="name"
                           label="Imię"
                           onChange={handleChange}
                           onBlur={handleBlur}
                           value={values.name}
                        />
                        <Input
                           type="text"
                           name="surname"
                           label="Nazwisko"
                           onChange={handleChange}
                           onBlur={handleBlur}
                           value={values.surname}
                        />
                        <Input
                           type="text"
                           name="index"
                           label="Indeks"
                           onChange={handleChange}
                           onBlur={handleBlur}
                           value={values.index}
                        />
                     </div>
                     <div className={styles.actions}>
                        <a href="/students" className={styles.button}>
                           Powrót
                        </a>
                        <Button type="submit" disabled={isSubmitting}>
                           Dodaj studenta
                        </Button>
                     </div>
                  </form>
               )}
            </Formik>
         </div>
      </div>
   );
};

export default Login;
