import React, { Component } from "react";
import styles from "./Students.module.scss";

import Button from "../../components/Button/Button";
import axios from "axios";

class Students extends Component {
   state = {
      students: [],
   };

   getStudentsHandler = function () {
      axios
         .get(`https://uwm-gr1.azurewebsites.net/api/Student`)
         .then((res) => {
            const students = res.data;
            this.setState({ students });
         })
         .catch((error) => {
            alert("Error: " + error.response.status);
         });
   };
   render() {
      return (
         <div className={styles.wrapper}>
            <Button onClick={() => this.getStudentsHandler()}>
               Pobierz dane
            </Button>
            <a href="/add-student" className={styles.button}>
               Dodaj studenta
            </a>
            <div className={styles.data}>
               <ul>
                  {this.state.students.map((student) => (
                     <li>
                        {student.name} {student.surname} {student.index}
                     </li>
                  ))}
               </ul>
            </div>
         </div>
      );
   }
}

export default Students;
