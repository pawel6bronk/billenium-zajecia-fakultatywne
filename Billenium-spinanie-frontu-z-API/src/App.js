import "./App.css";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import AddStudent from "./views/AddStudent/AddStudent";
import Students from "./views/Students/Students";

function App() {
   return (
      <BrowserRouter>
         <Switch>
            <Route exact path="/" render={() => <Redirect to="students" />} />
            <Route exact path="/students" component={Students} />
            <Route exact path="/add-student" component={AddStudent} />
         </Switch>
      </BrowserRouter>
   );
}

export default App;
